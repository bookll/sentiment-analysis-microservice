# Sentiment Microservice #

Microservice to analyse the setiment of a piece of text, using the Google Natural Language API. 

http://sentiment-microservice.herokuapp.com

### What is this repository for? ###

* Pass text in to a URL via query string to receive a natural language analysis
* Version 1.0

### How do I get set up? ###

* Clone repo
* Install deps
* Rename .env-sample to .env and add your own Google API key
* NPM start

### Params ###

* text [String]
* type [analyzeSyntax, annotateText, analyzeEntities, analyzeSentiment (default) ]

### Example usage ###


```
#!js

axios.get('http://sentiment-microservice.herokuapp.com', {
    params: {
      text: "I love starwars. I think it's great",
      type: "analyzeSentiment"

    }
  })
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error);
  });  
```

### Result ###


```
#!json

{
    "documentSentiment": {
        "polarity": 1,
        "magnitude": 1.5,
        "score": 0.7
    },
    "language": "en",
    "sentences": [
        {
            "text": {
                "content": "I love starwars.",
                "beginOffset": 0
            },
            "sentiment": {
                "polarity": 1,
                "magnitude": 0.8,
                "score": 0.8
            }
        },
        {
            "text": {
                "content": "I think it's great",
                "beginOffset": 17
            },
            "sentiment": {
                "polarity": 1,
                "magnitude": 0.6,
                "score": 0.6
            }
        }
    ]
}
```