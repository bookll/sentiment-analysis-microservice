require('dotenv').config()

const { send } = require('micro')
const url = require('url')
const googleNlp = require('google-nlp')
const apiKey = process.env.KEY

let visits = 0;

module.exports = function(req, res) {

    // Pass true as the second argument to also parse the query string using the querystring module. 

    const   path = url.parse(req.url, true),
            type = path.query.analysis || "analyzeSentiment",
            text = path.query.text || "You haven't passed the service any text. Bad boy!"

    visits++;

    let nlp = new googleNlp(apiKey);

    // Available methods are: analyzeSyntax, annotateText, analyzeEntities, analyzeSentiment

    nlp[type](text).then(data => {

        const sentiment = JSON.stringify(data, null, 4);
        res.write(sentiment);
        send(res, 200)

        console.log(`This service has been accessed ${visits} times today.`);

    });

}
